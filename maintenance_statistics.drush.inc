<?php
/**
 * @file
 * The drush file.
 */

/**
 * Implements hook_drush_help().
 */
function maintenance_statistics_drush_help($section) {

}

/**
 * Implements hook_drush_command().
 */
function maintenance_statistics_drush_command() {

  $items = array();

  $items['stats-modules'] = array(
    'description' => 'Get some statistics about modules',
    'callback' => 'maintenance_statistics_stats_modules'
  );

  $items['stats-views'] = array(
    'description' => 'Get some statistics about views',
    'drupal dependencies' => array('views'),
    'callback' => 'maintenance_statistics_stats_views'
  );

  return $items;
}

/**
 * Get modules statistics.
 */
function maintenance_statistics_stats_modules() {

  // Get stats.
  $total_modules = db_query("SELECT * FROM {system} WHERE type = 'module'")->rowCount();
  $active_modules = db_query("SELECT * FROM {system} WHERE type = 'module' AND status = 1")->rowCount();

  drush_print(t('Total number of modules: @number', array('@number' => $total_modules)));
  drush_print(t('Total number of active modules: @number', array('@number' => $active_modules)));

}

/**
 * Get views statistics.
 */
function maintenance_statistics_stats_views() {

  // Get stats.
  $total_display = db_query("SELECT * FROM {views_display}")->rowCount();
  $php_display = db_query("SELECT * FROM {views_display} WHERE display_options LIKE '%php%'")->rowCount();

  drush_print(t('Total number of display views: @number', array('@number' => $total_display)));
  drush_print(t('Total number of display views with PHP: @number', array('@number' => $php_display)));
}


